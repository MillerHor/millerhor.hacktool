// ==UserScript==
// @name         NineYi.SG.PullRequest.SelfCheckList
// @namespace    https://bitbucket.org/MillerHor/millerhor.hacktool/
// @version      0.4.4
// @description  try to take over the world!
// @author       MillerHor
// @match        https://bitbucket.org/nineyi/*/pull-requests/*
// @grant        none
// @require      https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js
// ==/UserScript==
(function () {
    'use strict';
    var nameSpace = 'nineyi_sg_pr_self_check_list';
    function buildBoard(ruleVersion) {
        var checkListBoard = $('<div style="position:fixed;bottom:5%;left:10px;background:#FFF;z-index:2999;border:1px solid rgb(223, 225, 230);border-radius:3px;padding:5px" />'),
            miniferButton = $('<div style="border:1px solid #DFE1E6;border-radius:3px;width:18px;height:18px;padding:1px;text-align:center;vertical-align:middle;float:right;cursor:pointer">-</div>');

        //// 最小化
        miniferButton.click(function () {
            if (checkListBoard.position().left < 0) {
                $(this).text('-');
                checkListBoard.animate({ left: '10px' });
            }
            else {
                var offsetLeft = 0 - checkListBoard.width() + 18;
                checkListBoard.animate({ left: offsetLeft + 'px' });
                $(this).text('+');
            }
        });

        //// 最小化
        checkListBoard.append(miniferButton);

        //// title
        checkListBoard.append('<h1 class="app-header--heading" style="margin-right:30px;"><span class="pull-request-title" style="white-space: nowrap;font-size:0.8em">PR Check List v' + ruleVersion + '</span></h1>');

        return checkListBoard;
    }

    function buildCheckList(ruleList, checkListBoard) {
        $.each(ruleList, function (idx) {
            var rule = this.rule,
                info = this.info;
            var id = nameSpace + '_' + idx;
            var ruleCheckBox = $('<input type="checkbox" id="' + id + '" name="' + rule + '" value="' + rule + '" />');
            var ruleLabel = $('<label for="' + id + '" class="description">' + rule + '</label>');
            var ruleItem = $('<div style="padding-bottom:2px"/>');

            ruleLabel.prop('data-info', info);
            ruleLabel.hover(function () {
                var $this = $(this);
                var message = $this.prop('data-info');
                var displayInfo = $('<span class="rule-label-info" style="position:absolute;border:1px solid #DFE1E6;border-radius:3px;box-shadow:5px 5px 3px #888;padding:5px;background:#FFF;white-space: nowrap;margin-left:5px;" />').text(message);
                $this.parent().append(displayInfo);
            }, function () {
                var $this = $(this);
                var displayInfo = $this.parent().find('.rule-label-info');
                displayInfo.remove();
            });

            ruleItem.append(ruleCheckBox);
            ruleItem.append(ruleLabel);
            checkListBoard.append(ruleItem);
        });


    }

    function buildSelectAllRuleItem(checkListBoard) {
        var id = nameSpace + '_checkAll'
        var selectAllRuleCheckBox = $('<input type="checkbox" id="' + id + '" name="' + id + '" value="' + id + '" />');
        var selectAllRuleLabel = $('<label for="' + id + '" class="description">全選</label>');
        var selectAllRuleItem = $('<div style="padding-bottom:2px"/>');

        selectAllRuleCheckBox.prop('isSelectAll', true).change(function () {
            var $this = $(this),
                isChecked = ($this.prop('checked') === true);
            checkListBoard.find('input[type="checkbox"]').prop('checked', isChecked);
        });

        selectAllRuleItem.append(selectAllRuleCheckBox);
        selectAllRuleItem.append(selectAllRuleLabel);
        checkListBoard.append(selectAllRuleItem);

    }

    function postComment(message) {
        //// /nineyi/nineyi.webstore.mobilewebmall/pull-requests/8638/feature-vsts12185-fix-printcode/diff
        var localPath = location.pathname.split('/');

        //// https://bitbucket.org/!api/1.0/repositories/nineyi/nineyi.webstore.mobilewebmall/pullrequests/8638/comments/
        var targetPath = ['https://bitbucket.org/!api/1.0/repositories', localPath[1], localPath[2], 'pullrequests', localPath[4], 'comments'].join('/');

        //// jQuery.post( url [, data ] [, success ] [, dataType ] )
        var postContent = { "content": message };
        var antiCSRFKey = $(document.getElementById('image-upload-template').innerText).find('[name="csrfmiddlewaretoken"]').val();

        $.ajax(targetPath, {
            headers: {
                'Accept': 'application/json, text/javascript, */*; q=0.01',
                'x-csrftoken': antiCSRFKey
            },
            method: 'POST',
            data: postContent,
            complete: function () {
                location.href = location.href;
            }
        });
    }

    function buildComfirmButton(checkListBoard) {
        var confirmButton = $('<button>確認完畢</button>').click(function () {
            var checkedItem = [];
            checkListBoard.find('input').each(function () {
                var checkItem = $(this);
                if (checkItem.prop('type') != 'checkbox') { return; }
                if (checkItem.prop('checked') === false) { return; }
                if (checkItem.prop('isSelectAll') === true) { return; }
                checkedItem.push(checkItem.val());
            });
            //console.log(checkedItem);
            postComment('* ' + checkedItem.join('\n* ') + '檢查完畢');
        });
        return confirmButton;
    }

    $(function () {
        $.getScript('https://bitbucket.org/MillerHor/millerhor.hacktool/raw/master/NineYi.SG.PullRequest.SelfCheckList/resource.js', function (data) {
            var codeSmellList = nineYi.sg.pr.selfCheckList.codeSmellList,
                basicList = nineYi.sg.pr.selfCheckList.basicList,
                ruleVersion = nineYi.sg.pr.selfCheckList.version;
            var checkListBoard = buildBoard(ruleVersion);

            //// 分隔線
            checkListBoard.append('<div style="border-bottom:1px solid #DFE1E6;margin:5px 0" />');

            buildCheckList(codeSmellList, checkListBoard);
            buildCheckList(basicList, checkListBoard);
            buildSelectAllRuleItem(checkListBoard);

            //// 分隔線
            checkListBoard.append('<div style="border-bottom:1px solid #DFE1E6;margin:5px 0" />');

            //// 確認按鈕
            var confirmButton = buildComfirmButton(checkListBoard);
            checkListBoard.append(confirmButton);

            $(document.body).append(checkListBoard);
        });

        // Your code here...
    });
})();