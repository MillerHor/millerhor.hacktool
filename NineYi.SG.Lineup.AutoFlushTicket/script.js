// ==UserScript==
// @name         NineYi.SG.Lineup.AutoFlushTicket
// @namespace    https://bitbucket.org/MillerHor/millerhor.hacktool/
// @version      0.1
// @description  try to take over the world!
// @author       MillerHor
// @match        http://*/V2/LineUp?status=CheckOut&display=true*
// @grant        none
// ==/UserScript==

(function() {
    'use strict';
    var flushTime = 5;
    var threadHold = 150;
    var countDown = flushTime;
    // Your code here...
    function flushQueue(){
        var queueLength = parseInt(/\d+/.exec($('.row').text())[0]);
        
        if(queueLength <= threadHold){
            location.href = location.href;
            return;
        }
        $('input').prop('checked', 'checked');
        $('.btn-release').trigger('click');
    }

    var autoFlush = setTimeout(function(){
        flushQueue();
    }, flushTime * 1000);
    var timer = null;
    var btn = $('<div style="cursor:pointer;background-color:rgb(0, 128, 0);color:#FFF;padding:5px;border:solid 1px #aaa;position:fixed;top:0;right:0;">Pause Flush</div>')
    .click(function(){
        var $this = $(this);
        if($this.css('background-color') == 'rgb(0, 128, 0)'){
            clearTimeout(autoFlush);
            autoFlush = null;
            clearTimeout(timer);
            timer = null;
            $(this).css('background-color', 'red').text('Flush NOW!');
        }
        else{
            flushQueue();
        }
    });
    timer = setInterval(function(){
        btn.text('Pause Flush(' + countDown + ')');
        countDown--;
    }, 1000);
    $(document.body).append(btn);

})();