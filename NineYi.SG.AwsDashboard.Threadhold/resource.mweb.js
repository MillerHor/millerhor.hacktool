var nineYi = {
        sg: {
            cloudwatch: {
                mweb: [
                    {
                        id :'widget-6',
                        meta:'G1 CPU',
                        sections: [
                            {
                                meta: 'MWeb1',
                                sId:'section-0',
                                threadHold : '70',
                                operator: '>'
                            },
                            {
                                meta: 'MWeb2',
                                sId:'section-1',
                                threadHold : '70',
                                operator: '>'
                            }
                        ]
                    },
                    {
                        id :'widget-7',
                        meta:'G2 CPU',
                        sections: [
                            {
                                meta: 'MWeb16',
                                sId:'section-0',
                                threadHold : '70',
                                operator: '>'
                            },
                            {
                                meta: 'MWeb17',
                                sId:'section-1',
                                threadHold : '70',
                                operator: '>'
                            }
                        ]
                    },
                    {
                        id :'widget-18',
                        meta:'G4 CPU',
                        sections: [
                            {
                                meta: 'MWeb31',
                                sId:'section-0',
                                threadHold : '70',
                                operator: '>'
                            },
                            {
                                meta: 'MWeb32',
                                sId:'section-1',
                                threadHold : '70',
                                operator: '>'
                            }
                        ]
                    },
                    {
                        id :'widget-19',
                        meta:'Data2 Redis',
                        sections: [
                            {
                                meta: 'FreeableMemory',
                                sId:'section-0',
                                threadHold : '2',
                                operator: '<'
                            },
                            {
                                meta: 'StringBasedCmds',
                                sId:'section-1',
                                threadHold : '1',
                                threadHoldUnit: 'm',
                                operator: '>'
                            },
                            {
                                meta: 'CPU',
                                sId:'section-2',
                                threadHold : '70',
                                operator: '>'
                            }
                        ]
                    },
                    {
                        id :'widget-9',
                        meta:'Data Redis',
                        sections: [
                            {
                                meta: 'FreeableMemory',
                                sId:'section-0',
                                threadHold : '2',
                                operator: '<'
                            },
                            {
                                meta: 'StringBasedCmds',
                                sId:'section-1',
                                threadHold : '1',
                                threadHoldUnit: 'm',
                                operator: '>'
                            },
                            {
                                meta: 'CPU',
                                sId:'section-2',
                                threadHold : '70',
                                operator: '>'
                            }
                        ]
                    },
                    {
                        id :'widget-12',
                        meta:'Cache Redis',
                        sections: [
                            {
                                meta: 'FreeableMemory',
                                sId:'section-0',
                                threadHold : '2',
                                operator: '<'
                            },
                            {
                                meta: 'StringBasedCmds',
                                sId:'section-1',
                                threadHold : '1',
                                threadHoldUnit: 'm',
                                operator: '>'
                            },
                            {
                                meta: 'CPU',
                                sId:'section-2',
                                threadHold : '70',
                                operator: '>'
                            }
                        ]
                    },
                    {
                        id :'widget-11',
                        meta:'OutputCache Redis',
                        sections: [
                            {
                                meta: 'FreeableMemory',
                                sId:'section-0',
                                threadHold : '2',
                                operator: '<'
                            },
                            {
                                meta: 'StringBasedCmds',
                                sId:'section-1',
                                threadHold : '1',
                                threadHoldUnit: 'm',
                                operator: '>'
                            },
                            {
                                meta: 'CPU',
                                sId:'section-2',
                                threadHold : '70',
                                operator: '>'
                            }
                        ]
                    },
                    {
                        id :'widget-8',
                        meta:'DynamoDB',
                        sections: [
                            {
                                meta: 'Index ReadThrottleEvents',
                                sId:'section-0',
                                threadHold : '1',
                                operator: '>'
                            },
                            {
                                meta: 'Table Query ThrottledRequests',
                                sId:'section-1',
                                threadHold : '1',
                                operator: '>'
                            }
                        ]
                    },
                    {
                        id :'widget-2',
                        meta:'Thirdparty API Request',
                        sections: [
                            {
                                meta: 'Request Count',
                                sId:'section-0',
                                threadHold : '10',
                                operator: '>'
                            }
                        ]
                    },
                    {
                        id :'widget-14',
                        meta:'UnHealthyHosts',
                        sections: [
                            {
                                meta: 'G1',
                                sId:'section-0',
                                threadHold : '1',
                                operator: '>'
                            },
                            {
                                meta: 'G2',
                                sId:'section-1',
                                threadHold : '1',
                                operator: '>'
                            },
                            {
                                meta: 'G3',
                                sId:'section-2',
                                threadHold : '1',
                                operator: '>'
                            },
                            {
                                meta: 'G4',
                                sId:'section-3',
                                threadHold : '1',
                                operator: '>'
                            }
                        ]
                    }
                ]}
        }
    };