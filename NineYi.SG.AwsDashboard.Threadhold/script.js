function beginAlerm(resource){
            setInterval(function(){
                $.each(resource, function(){
                    var iiiiid = this.id;
                    var itemBlock = $('div[data-reactid$="' + this.id + '"]');
                    if(itemBlock.length === 0){return;}
                    $.each(this.sections, function(){
                        var sectionBlock = itemBlock.find('div[data-reactid$="' + this.sId + '"]');
                        var currentThreadBlock = sectionBlock.find('div.cwdb-single-value-number-value');
                        var currentThread = parseFloat(currentThreadBlock.text());
                        var currentThreadUnit = sectionBlock.find('div.cwdb-single-value-number-info-unit').text();
                        var unitIsEqual = false;
                        
                        if(typeof(this.originFontColor)=== 'undefined'){
                            this.originFontColor = sectionBlock.css('color');
                        }

                        if(typeof(this.threadHoldUnit) === 'undefined'){
                            unitIsEqual = true;
                        }
                        else if(this.threadHoldUnit === ''){
                            unitIsEqual = true;
                        }
                        else if(currentThreadUnit === this.threadHoldUnit){
                            unitIsEqual = true;
                        }

                        var isOverThreadhold = false;
                        if(unitIsEqual && this.operator === '>'){
                            isOverThreadhold = currentThread >= this.threadHold;
                        }
                        else if(unitIsEqual && this.operator === '<'){
                            isOverThreadhold = currentThread <= this.threadHold;
                        }
                        else{

                        }

                        if(isOverThreadhold){
                            if(this.blink == null || typeof(this.blink) === 'undefined'){
                                this.blink = setInterval(function(){


                                    currentThreadBlock.css('color', '#ff0000');
                                    setTimeout(function(){
                                        currentThreadBlock.css('color', '#444444');
                                    }, 600);
                                }, 1200);
                            }
                        }
                        else{
                            if(this.blink){
                                clearInterval(this.blink);
                                this.blink = null;
                                currentThreadBlock.css('color', this.originFontColor);
                            }
                        }
                    });
                });
            }, 1000);

            var notifyBlock = $('<div style="position: fixed;top: 110px;right:0;z-index: 9999;background: #FF0000;font-weight: bolder;padding: 5px;color: #FFF;">Thread Hold Plugin is enabled</div>');
            $(document.body).append(notifyBlock);
            notifyBlock.fadeOut(3000);
        }